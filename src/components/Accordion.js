import React, {useState} from "react";

const Accordion = props => {
    const [active, setActive] = useState();
    const style = {
        textAlign: "left",
        fontSize: '16px'
    }
    const styleBtn = {
        color: 'red',
        borderRadius: '20px',
        border: '1px solid black',
        padding: '10px 20px'
    }
    return (
        <div>
            <button style={styleBtn} onClick={() => setActive(!active)}>{props.name}</button>
            {active && 
            <div style={style}>
                <div>
                    {props.username}
                </div>
                <div>
                    {props.email}
                </div>
                <div>
                    {props.phone}
                </div>
                <div>
                    {props.website}
                </div>
            </div>}
        </div>
    )
}


export default Accordion;