import React, {useEffect, useState} from 'react';
import './App.css';
import Accordion from './components/Accordion';

function App() {
  const [active, setActive] = useState();
  const [items, setItems] = useState([])

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(json => setItems(json))
  },[active])

  const style = {
    border: '1px solid black',
    width: '300px',
    margin: '0 auto',
    background: 'gray',
    padding: '10px 50px'
  }
  
  return (
    <div className="App">
        <div style={style}>
          {items.length &&
            items.map(item => 
            <pre key = {item.id}>
                <Accordion
                  name ={item.name}
                  username={'name: ' + item.username}
                  email={'email: ' + item.email}
                  phone={'phone: ' + item.phone}
                  website={'website: ' + item.website}
                />
              </pre>)
          }
        </div>
    </div> 
  );
}

export default App;
